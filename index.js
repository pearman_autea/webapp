//Lets require/import the HTTP module
var app = require('express')();

//Lets define a port we want to listen to
const PORT = process.env.PORT | 8080; 

//We need a function which handles requests and send response
app.get('/api', (req, res) => {
    res.end('It Works!!');
});

//Lets start our server
app.listen(PORT, function(){
    console.log('PORT : ' + process.env.PORT);
    //Callback triggered when server is successfully listening. Hurray!
    console.log("Server listening on: http://localhost:%s", PORT);
});

module.exports = app;